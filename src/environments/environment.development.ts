export const environment = {
  production: false,
  catalogo: 'http://localhost:8082',
  eventos: 'http://localhost:8081',
  video: 'http://localhost:8080/videos'
};
