export const environment = {
  production: true,
  catalogo: `${window.location.protocol}//${window.location.host}`,
  eventos: `${window.location.protocol}//${window.location.host}`,
  video: `${window.location.protocol}//${window.location.host}/videos`,
};
