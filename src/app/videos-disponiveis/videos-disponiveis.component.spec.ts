import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideosDisponiveisComponent } from './videos-disponiveis.component';

describe('VideosDisponiveisComponent', () => {
  let component: VideosDisponiveisComponent;
  let fixture: ComponentFixture<VideosDisponiveisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [VideosDisponiveisComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VideosDisponiveisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
