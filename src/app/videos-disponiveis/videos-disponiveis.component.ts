import {Component} from '@angular/core';
import {VideoAvailableService} from "../services/video-available.service";
import {VideoAvailable} from "../entity/video-available";
import {DatePipe, NgClass, NgForOf} from "@angular/common";

@Component({
  selector: 'app-videos-disponiveis',
  standalone: true,
  imports: [
    DatePipe,
    NgForOf,
    NgClass
  ],
  templateUrl: './videos-disponiveis.component.html',
  styleUrl: './videos-disponiveis.component.css'
})
export class VideosDisponiveisComponent {
  videos: VideoAvailable[] = [];
  currentPage = 1; // Página atual
  pageSize = 10; // Quantidade de vídeos por página
  totalItems = 0; // Total de vídeos (obtido da API)
  totalPages = 0

  constructor(private videoAvailableService: VideoAvailableService) {
  }

  ngOnInit() {
    this.loadVideos();
  }

  loadVideos() {
    this.videoAvailableService.getVideos(this.currentPage - 1, this.pageSize)
      .subscribe(response => {
        this.videos = response.content; // Assumindo que a resposta tem um array 'content' com os vídeos
        this.totalItems = response.totalElements; // Assumindo que a resposta tem o total de elementos
        this.currentPage = response.pageable.pageNumber + 1;
        this.totalPages = response.totalPages;
      });
  }

  getVideoUrl(video: VideoAvailable): string {
    return this.videoAvailableService.createURLVideo(video);
  }

  changePage(page: number) {
    if (page > 0 && page <= this.totalPages) {
      this.currentPage = page;
      this.loadVideos();
    }
  }
}
