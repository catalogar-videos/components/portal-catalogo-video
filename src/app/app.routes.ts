import {RouterModule, Routes} from '@angular/router';
import {CapturaVideoComponent} from "./captura-video/captura-video.component";
import {VisualizarEventosComponent} from "./visualizar-eventos/visualizar-eventos.component";
import {VideosDisponiveisComponent} from "./videos-disponiveis/videos-disponiveis.component";
import {NgModule} from "@angular/core";

export const routes: Routes = [
  {path: 'captura-video', component: CapturaVideoComponent},
  {path: 'visualizar-eventos', component: VisualizarEventosComponent},
  {path: 'videos-disponiveis', component: VideosDisponiveisComponent},
  {path: '', redirectTo: '/captura-video', pathMatch: 'full'} // Rota padrão
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
