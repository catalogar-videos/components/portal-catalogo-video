import {TestBed} from '@angular/core/testing';

import {CapturaVideoService} from './captura-video.service';

describe('CapturaVideoService', () => {
  let service: CapturaVideoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CapturaVideoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
