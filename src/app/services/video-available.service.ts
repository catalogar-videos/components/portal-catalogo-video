import {Injectable} from '@angular/core';
import {BaseURLService} from "./base-url.service";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PagedVideoResponse, VideoAvailable} from "../entity/video-available";

@Injectable({
  providedIn: 'root'
})
export class VideoAvailableService {

  constructor(private http: HttpClient, private baseURL: BaseURLService) {
  }

  getVideos(page: number = 0, size: number = 10, sortBy: string = 'createDate', sortDirection: string = 'desc'): Observable<PagedVideoResponse> {

    const params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sortBy', sortBy)
      .set('sortDirection', sortDirection);

    return this.http.get<PagedVideoResponse>(this.baseURL.createUrlCatalogo("catalog/videos"), {params});

  }

  createURLVideo(video: VideoAvailable): string {
    const path = this.videoName(video.videoFilePath);
    return this.baseURL.createURLVideo(path);
  }

  private videoName(path: string): string {
    const match = path.match(/[\w-]+\.mp4$/i);
    return match ? match[0] : path;
  }
}
