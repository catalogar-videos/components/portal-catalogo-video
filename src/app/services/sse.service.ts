import {Injectable, NgZone} from '@angular/core';
import {BaseURLService} from "./base-url.service";
import {Observable} from "rxjs";
import {Event} from "../entity/event";

@Injectable({
  providedIn: 'root'
})
export class SseService {

  constructor(private zone: NgZone, private baseURL: BaseURLService) {
  }

  getServerSentEvent(): Observable<Event> {
    return new Observable<Event>(observer => {
      const path = this.baseURL.createURLEventos("events");
      const eventSource = new EventSource(path);

      eventSource.onmessage = (event: MessageEvent) => {
        this.zone.run(() => {
          const data: Event = JSON.parse(event.data); // Assumindo que a resposta é um JSON válido
          observer.next(data);
        });
      };

      eventSource.onerror = (error) => {
        this.zone.run(() => {
          observer.error(error);
        });
      };

      return () => {
        eventSource.close();
      };
    });
  }

}
