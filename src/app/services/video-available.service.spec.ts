import { TestBed } from '@angular/core/testing';

import { VideoAvailableService } from './video-available.service';

describe('VideoAvailableService', () => {
  let service: VideoAvailableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoAvailableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
