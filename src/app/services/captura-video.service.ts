import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VideoSource, VideoSourcePayload} from "../entity/video-source";
import {Observable} from "rxjs";
import {BaseURLService} from "./base-url.service";

@Injectable({
  providedIn: 'root'
})
export class CapturaVideoService {

  constructor(private http: HttpClient, private baseURL: BaseURLService) {
  }

  getParameters(): Observable<VideoSource[]> {
    return this.http.get<VideoSource[]>(this.baseURL.createUrlCatalogo("catalog/videos/parameters"));
  }

  captureVideo(videoSource: VideoSourcePayload): Observable<undefined> {
    return this.http.post<undefined>(this.baseURL.createUrlCatalogo("catalog/videos"), videoSource);
  }
}
