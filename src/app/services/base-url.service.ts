import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BaseURLService {

  constructor() {
  }

  createUrlCatalogo(url: string): string {
    return environment.catalogo.concat(`/${url}`);
  }

  createURLEventos(url: string): string {
    return environment.eventos.concat(`/${url}`);
  }

  createURLVideo(path: string): string {
    return environment.video.concat(`/${path.split("/")[2]}`);
  }

}
