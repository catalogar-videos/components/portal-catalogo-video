import {Component} from '@angular/core';
import {DatePipe, NgForOf} from "@angular/common";
import {SseService} from "../services/sse.service";
import {Event} from "../entity/event";

@Component({
  selector: 'app-visualizar-eventos',
  standalone: true,
  imports: [
    NgForOf,
    DatePipe
  ],
  templateUrl: './visualizar-eventos.component.html',
  styleUrl: './visualizar-eventos.component.css'
})
export class VisualizarEventosComponent {

  events: Event[] = [];

  constructor(private sseService: SseService) {
  }

  ngOnInit() {
    this.sseService.getServerSentEvent().subscribe(event => this.events.unshift(event));
  }

}
