export interface PagedVideoResponse {
  content: VideoAvailable[];  // Array de vídeos da página atual
  pageable: {
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    pageNumber: number;
    pageSize: number;
    offset: number;
    paged: boolean;
    unpaged: boolean;
  };
  totalPages: number;
  totalElements: number;
  last: boolean;
  first: boolean;
  numberOfElements: number;
  size: number;
  number: number;
  empty: boolean;
}

export interface VideoAvailable {

  videoFilePath: string;

  metadata: Metadata;
}

export interface Metadata {

  author: string;

  createDate: Date;

  name: string;

  tags: string[];

}
