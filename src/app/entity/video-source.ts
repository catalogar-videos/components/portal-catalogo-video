export interface VideoSource {
  alias: string;
  name: string;
  parameters: string[];
}

export interface VideoSourcePayload {
  alias: string;
  parameters: { [key: string]: string };
}
