export interface EventInfo {
  key: string;
  value: string;
}

export interface Event {
  timestamp: Date;
  id: string;
  event: string;
  infos: EventInfo[];
}

