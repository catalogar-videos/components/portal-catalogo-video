import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapturaVideoComponent } from './captura-video.component';

describe('CapturaVideoComponent', () => {
  let component: CapturaVideoComponent;
  let fixture: ComponentFixture<CapturaVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CapturaVideoComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CapturaVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
