// captura-video.component.ts
import {Component, OnInit} from '@angular/core';
import {CapturaVideoService} from '../services/captura-video.service';
import {VideoSource, VideoSourcePayload} from '../entity/video-source';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {NgForOf, NgIf} from "@angular/common";

@Component({
  selector: 'app-captura-video',
  standalone: true,
  imports: [ReactiveFormsModule, NgForOf, NgIf],
  templateUrl: './captura-video.component.html',
  styleUrls: ['./captura-video.component.css']
})
export class CapturaVideoComponent implements OnInit {
  videoSources: VideoSource[] = [];
  videoForm: FormGroup;
  selectedVideoSource: VideoSource | null = null;

  constructor(private service: CapturaVideoService, private fb: FormBuilder) {
    this.videoForm = this.fb.group({
      videoSource: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.service.getParameters().subscribe(videoSources => {
      this.videoSources = videoSources;
    });
  }

  onVideoSourceChange(event: Event) {
    const selectedSourceName = (event.target as HTMLSelectElement).value;
    this.selectedVideoSource = this.videoSources.find(source => source.name === selectedSourceName) || null;

    // Remove all previous dynamic controls
    this.removeDynamicControls();

    if (this.selectedVideoSource) {
      this.selectedVideoSource.parameters.forEach(param => {
        this.videoForm.addControl(param, this.fb.control('', Validators.required));
      });
    }
  }

  private removeDynamicControls() {
    Object.keys(this.videoForm.controls).forEach(control => {
      if (control !== 'videoSource') {
        this.videoForm.removeControl(control);
      }
    });
  }

  onSubmit() {
    if (this.videoForm.valid && this.selectedVideoSource) {
      const formData: any = this.videoForm.value;
      const parameters: { [key: string]: string } = {}; // Objeto parameters vazio
      Object.keys(formData).forEach(key => {
        parameters[key] = formData[key]; // Adiciona cada chave/valor do formData ao objeto parameters
      });
      const payload: VideoSourcePayload = {
        alias: this.selectedVideoSource.alias,
        parameters: parameters
      };
      this.service.captureVideo(payload).subscribe();
    }
  }
}
