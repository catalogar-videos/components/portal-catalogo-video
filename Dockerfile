# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM node:21 as build

# Set the working directory
WORKDIR /app

# Add the `node_modules` folder to `.dockerignore` to copy only relevant files
COPY package*.json ./

# Install all dependencies
RUN npm install

# Copy the remaining files
COPY . .

# Generate build of the application
RUN npm run build

## Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:latest

# Delete existing nginx configurations
RUN rm -rf /etc/nginx/conf.d

# Copy the custom default nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# From ‘build’ stage copy over the artifacts to nginx server root folder
COPY --from=build /app/dist/portal-catalogo/browser /usr/share/nginx/html

# Expose port 80
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
